package id.teguh.training.dashboard.controller;

import javax.servlet.http.HttpServletRequest;

public class BaseController {
    protected String getAppUrl(HttpServletRequest request) {
        return "http://" + request.getServerName() + ":" + request.getServerPort() + request.getContextPath();
    }
}
