package id.teguh.training.dashboard.controller;

import id.teguh.training.dashboard.dao.PasswordResetTokenDao;
import id.teguh.training.dashboard.dao.UserDao;
import id.teguh.training.dashboard.entity.PasswordResetToken;
import id.teguh.training.dashboard.entity.User;
import id.teguh.training.dashboard.service.MailService;
import id.teguh.training.dashboard.service.PasswordResetTokenService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

@Controller
public class LoginController extends BaseController {

    private final Logger LOGGER = LoggerFactory.getLogger(this.getClass());

    @Autowired private UserDao userDao;
    @Autowired private PasswordResetTokenDao passwordResetTokenDao;
    @Autowired private MailService mailService;
    @Autowired private PasswordResetTokenService passwordResetService;
    @Autowired private PasswordEncoder passwordEncoder;

    @GetMapping("/login")
    public String loginPage() {
        return "login";
    }

    @GetMapping(value = "/forgot_password")
    public String forgotPasswordPage() {
        return "forgot_password";
    }

    @PostMapping(value = "/forgot_password")
    public String saveForgotPassword(@RequestParam("email") String email, Model model, HttpServletRequest request) {
        Map<String, String> msg = new HashMap<>();

        if (!StringUtils.hasText(email)) {
            LOGGER.error("Forgot Password Error, Email is null ");
            msg.put("EmailIsNull", "Email can/t be empty");
        }

        User user = userDao.findByEmail(email);

        if (user == null) {
            LOGGER.error("Forgot Password Error, USER NOT FOUND WITH USERNAME : [{}] ", email);
            msg.put("UserNotFound", "Email not valid");
        } else {
            String token = UUID.randomUUID().toString().replaceAll("-", "");
            PasswordResetToken passwordResetToken = passwordResetTokenDao.findByUser(user);

            if (passwordResetToken == null) {
                passwordResetToken = new PasswordResetToken(token, user);
            } else {
                passwordResetToken.setToken(token);
                passwordResetToken.setExpiryDate(passwordResetToken.calculateExpiryDate());
            }

            passwordResetTokenDao.save(passwordResetToken);

            String link = getAppUrl(request) + "/reset_password?token=" + token;

            try {
                mailService.sendEmail(user.getEmail(), "Reset Password", "Link Reset Password : <a href='" + link + "'>" + token + "</a>");
                msg.put("success", "Reset Link had been sent to email");
            } catch (Exception ex) {
                LOGGER.error(ex.getMessage());
                msg.put("ErrorSendEmail", "Error sent email: " + ex.getMessage());
            }
        }
        model.addAttribute("message", msg);
        return "forgot_password";
    }

    @GetMapping(value = "/reset_password")
    public String getResetPassword(@RequestParam String token, Model model, RedirectAttributes redir) {
        Map<String, String> message = new HashMap<>();

        if (!StringUtils.hasText(token)) {
            LOGGER.error("Error Reset Password, token is null");
            message.put("ErrorTokenNull", "{Token Is NUll}");
            redir.addFlashAttribute("message", message);
            return "redirect:signin";
        }

        String result = passwordResetService.validatePasswordResetToken(token);
        if (result != null) {
            message.put("ErrResetToken", result);
            redir.addFlashAttribute("message", message);
            return "redirect:signin";
        }

        model.addAttribute("message", message);
        return "reset_password";
    }

    @PostMapping(value = "/reset_password")
    public String resetPassword(String newPassword, String token, Model model, RedirectAttributes redirectAttributes) {
        Map<String, String> message = new HashMap<>();

        if (!StringUtils.hasText(newPassword) && !StringUtils.hasText(token)) {
            LOGGER.error("Error Reset Password, PASSWORD OR TOKEN IS NULL");
            message.put("TokenOrPasswordNull", "Terjadi Kesalahan Dalam Reset Password");
            redirectAttributes.addFlashAttribute("message", message);
            return "redirect:signin";
        }

        PasswordResetToken passwordResetToken = passwordResetTokenDao.findByToken(token);

        if (passwordResetToken == null) {
            message.put("tokenNofFound", "token tidak ditemukan / akses token sudah kadaluarsa");
            model.addAttribute(message);
            return "reset_password?token=" + token;
        }

        User u = passwordResetToken.getUser();
        u.getUserPassword().setPassword(passwordEncoder.encode(newPassword));
        userDao.save(u);
        passwordResetTokenDao.delete(passwordResetToken);
        message.put("success", "Password Anda Berhasil di Rubah, Silahkan Login Kembali");
        redirectAttributes.addFlashAttribute("message", message);

        return "redirect:";
    }
}
