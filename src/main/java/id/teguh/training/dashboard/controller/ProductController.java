package id.teguh.training.dashboard.controller;

import id.teguh.training.dashboard.dto.Product;
import id.teguh.training.dashboard.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.servlet.ModelAndView;

import javax.validation.Valid;
import java.io.IOException;


@Controller
@RequestMapping("/products")
public class ProductController {

    @Autowired
    private ProductService productService;

    @GetMapping("/list")
    public String listPage(@PageableDefault Pageable pageable, @RequestParam(required = false) String name, Model model) throws IOException {
        model.addAttribute("products", productService.list(pageable, name));
        model.addAttribute("name", name);
        return "/master/product/list";
    }

    @GetMapping("/form")
    public String getProductForm(@RequestParam(value = "id", required = false) String id, Model model) {
        Product product = StringUtils.isEmpty(id) ? new Product() : productService.get(id);
        model.addAttribute("product", product);
        return "/master/product/form";
    }

    @PostMapping("/form")
    public String saveProduct(@ModelAttribute @Valid Product product, BindingResult bindingResult, SessionStatus status, Model model) {
        if (bindingResult.hasErrors()) {
            model.addAttribute("product", product);
            return "/master/product/form";
        }
        if (StringUtils.isEmpty(product.getId())) {
            product = productService.create(product);
        } else {
            productService.update(product.getId(), product);
        }
        status.setComplete();
        return "redirect:/products/form?id=" + product.getId();
    }

    @GetMapping("/delete")
    public String deleteConfirm(@RequestParam String id, Model model) {
        model.addAttribute("product", productService.get(id));
        return "/master/product/delete";
    }

    @PostMapping("/delete")
    public Object deleteProduct(@ModelAttribute Product product, SessionStatus status) {
        try {
            productService.delete(product.getId());
        } catch (DataIntegrityViolationException exception) {
            status.setComplete();
            return new ModelAndView("error/errorHapus")
                    .addObject("entityId", product.getName())
                    .addObject("entityName", "Product")
                    .addObject("errorCause", exception.getRootCause().getMessage())
                    .addObject("backLink", "/products/list");
        }
        status.setComplete();
        return "redirect:/products/list";
    }
}
