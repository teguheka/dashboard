package id.teguh.training.dashboard.controller;

import id.teguh.training.dashboard.dao.RoleDao;
import id.teguh.training.dashboard.dao.UserDao;
import id.teguh.training.dashboard.entity.User;
import id.teguh.training.dashboard.entity.UserPassword;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.servlet.ModelAndView;

import javax.validation.Valid;
import java.util.Objects;

@Controller
@RequestMapping("/users")
public class UserController extends BaseController {

    @Autowired private UserDao userDao;
    @Autowired private RoleDao roleDao;

    @GetMapping("/list")
    public String listPage(@PageableDefault Pageable pageable, @RequestParam(required = false) String username, Model model) {
        if (StringUtils.isEmpty(username)) {
            model.addAttribute("users", userDao.findAll(pageable));
        } else {
            model.addAttribute("users", userDao.findByUsernameContainingIgnoreCase(username, pageable));
        }

        model.addAttribute("username", username);
        return "/master/user/list";
    }

    @GetMapping("/form")
    public String getUserForm(Model model) {
        model.addAttribute("roles", roleDao.findAll());
        model.addAttribute("user", new User());
        return "/master/user/form";
    }

    @PostMapping("/form")
    public Object saveUser(@ModelAttribute @Valid User user, BindingResult bindingResult, SessionStatus status, Model model) {

        model.addAttribute("user", user);
        model.addAttribute("roles", roleDao.findAll());

        if (bindingResult.hasErrors()) {
            return "/master/user/form";
        }

        if (!Objects.equals(user.getPassword(), user.getConfirmedPassword())) {
            bindingResult.addError(new FieldError("password", "password", "didn't match"));
            return "/master/user/form";
        }

        User userByUsername = userDao.findByUsername(user.getUsername());
        if (userByUsername != null) {
            bindingResult.addError(new FieldError("username", "username", "Username already exist"));
            return "/master/user/form";
        }

        User userByEmail = userDao.findByEmail(user.getEmail());
        if (userByEmail != null) {
            bindingResult.addError(new FieldError("email", "email", "Email already used by other username"));
            return "/master/user/form";
        }

        user.setId(user.getUsername());
        user.setUserPassword(new UserPassword(user, user.getPassword()));

        try {
            userDao.save(user);
            status.setComplete();
        } catch (DataIntegrityViolationException exception) {
            return new ModelAndView("error/errorAdd")
                    .addObject("entityId", user.getUsername())
                    .addObject("entityName", User.class)
                    .addObject("errorCause", exception.getRootCause().getMessage())
                    .addObject("backLink", "/users/list");
        }

        return "redirect:/users/list";
    }
}
