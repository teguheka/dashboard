package id.teguh.training.dashboard.controller.api;

import com.google.common.base.Joiner;
import id.teguh.training.dashboard.dao.GenericSpecificationsBuilder;
import id.teguh.training.dashboard.dao.UserDao;
import id.teguh.training.dashboard.dao.UserSpecification;
import id.teguh.training.dashboard.dao.UserSpecificationsBuilder;
import id.teguh.training.dashboard.entity.User;
import id.teguh.training.dashboard.util.CriteriaParser;
import id.teguh.training.dashboard.util.SearchOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

@RestController
@RequestMapping("api/v1/users")
public class UserApiController {
    @Autowired
    private UserDao userDao;

    //http://localhost:8080/api/v1/users?search=username:teguh,active:Boolean.TRUE
    @GetMapping("")
    public Page<User> findAllByAndPredicate(Pageable pageable, @RequestParam String search) {
        Specification<User> spec = resolveSpecification(search);
        return userDao.findAll(spec, pageable);
    }

    //http://localhost:8080/api/v1/users/adv?search=( username:teguh OR username:andi ) OR id:admin
    @GetMapping(value = "/adv")
    public Page<User> findAllByAdvPredicate(Pageable pageable, @RequestParam(value = "search") String search) {
        Specification<User> spec = resolveSpecificationFromInfixExpr(search);
        return userDao.findAll(spec, pageable);
    }

    protected Specification<User> resolveSpecification(String searchParameters) {
        UserSpecificationsBuilder builder = new UserSpecificationsBuilder();
        String operationSetExper = Joiner.on("|").join(SearchOperation.SIMPLE_OPERATION_SET);
        Pattern pattern = Pattern.compile("(\\p{Punct}?)(\\w+?)(" + operationSetExper + ")(\\p{Punct}?)(\\w+?)(\\p{Punct}?),");
        Matcher matcher = pattern.matcher(searchParameters + ",");
        while (matcher.find()) {
            builder.with(matcher.group(1), matcher.group(2), matcher.group(3), matcher.group(5), matcher.group(4), matcher.group(6));
        }
        return builder.build();
    }

    protected Specification<User> resolveSpecificationFromInfixExpr(String searchParameters) {
        CriteriaParser parser = new CriteriaParser();
        GenericSpecificationsBuilder<User> specBuilder = new GenericSpecificationsBuilder<>();
        return specBuilder.build(parser.parse(searchParameters), UserSpecification::new);
    }
}
