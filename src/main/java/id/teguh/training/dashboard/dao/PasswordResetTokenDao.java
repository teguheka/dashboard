package id.teguh.training.dashboard.dao;

import id.teguh.training.dashboard.entity.PasswordResetToken;
import id.teguh.training.dashboard.entity.User;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Date;

@Repository
public interface PasswordResetTokenDao extends PagingAndSortingRepository<PasswordResetToken, String> {

    PasswordResetToken findByUser(User user);

    PasswordResetToken findByToken(String token);

    @Modifying
    @Query("DELETE PasswordResetToken prt WHERE prt.expiryDate <= :now")
    void deleteExpiredToken(@Param("now") Date date);
}