package id.teguh.training.dashboard.dao;

import id.teguh.training.dashboard.entity.Role;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RoleDao extends PagingAndSortingRepository<Role, String> {

}
