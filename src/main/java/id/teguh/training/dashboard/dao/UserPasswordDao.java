package id.teguh.training.dashboard.dao;

import id.teguh.training.dashboard.entity.UserPassword;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface UserPasswordDao extends PagingAndSortingRepository<UserPassword, String> {

    public Optional<UserPassword> findById(String userId);
}