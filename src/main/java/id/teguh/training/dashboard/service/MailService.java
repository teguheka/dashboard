package id.teguh.training.dashboard.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import java.util.HashMap;
import java.util.Map;

@Service
public class MailService {
    private Logger LOGGER = LoggerFactory.getLogger(MailService.class);

    @Autowired private RestTemplate restTemplate;

    @Value("${gmail.sender.name}")
    private String mailSenderName;

    @Value("${gmail.service.url}")
    private String mailServiceUrl;

    private HttpHeaders getHttpHeaders() {
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.setContentType(MediaType.APPLICATION_JSON);
        return httpHeaders;
    }

    private Object parse(ResponseEntity<String> responseEntity) {
        if (HttpStatus.OK == responseEntity.getStatusCode()) {
            try {
                ObjectMapper mapper = new ObjectMapper();
                return mapper.readValue(responseEntity.getBody(), HashMap.class);
            } catch (Exception e) {
                throw new RuntimeException("JsonException, message: " + e.getMessage());
            }
        } else {
            throw new RuntimeException("HttpStatus: " + responseEntity.getStatusCode() + ", error: " + responseEntity.getBody());
        }
    }

    public void sendEmail(String to, String subject, String content) {
        LOGGER.info("send email to {} with content {}", to, content);
        Map<String, String> params = new HashMap<>();
        params.put("from", mailSenderName);
        params.put("to", to);
        params.put("subject", subject);
        params.put("content", content);

        ResponseEntity<String> responseEntity = restTemplate.exchange(mailServiceUrl + "/api/v1/mail/send", HttpMethod.POST, new HttpEntity(params, getHttpHeaders()), String.class);
        Object data = parse(responseEntity);
        LOGGER.info("result send email:" + data);
    }


}
