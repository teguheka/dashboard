package id.teguh.training.dashboard.service;

import id.teguh.training.dashboard.dao.PasswordResetTokenDao;
import id.teguh.training.dashboard.entity.PasswordResetToken;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

@Service
@Transactional
@EnableScheduling
public class PasswordResetTokenService {

    @Autowired
    private PasswordResetTokenDao passwordResetTokenDao;

    private Logger logger = LoggerFactory.getLogger(PasswordResetTokenService.class);
    private final SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");

    public String validatePasswordResetToken(String token) {
        PasswordResetToken passToken = passwordResetTokenDao.findByToken(token);
        if (passToken == null) {
            return "Token not found";
        }
        Calendar cal = Calendar.getInstance();
        if ((passToken.getExpiryDate().getTime() - cal.getTime().getTime()) <= 0) {
            return "Token has been expired";
        }
        return null;
    }

    @Scheduled(cron = "0 1 0 * * ?")
    public void reportCurrentTime() {
        long now = System.currentTimeMillis();
        logger.info("Scheduler run on {}", dateFormat.format(new Timestamp(now)));

        passwordResetTokenDao.deleteExpiredToken(new Date());
    }
}
