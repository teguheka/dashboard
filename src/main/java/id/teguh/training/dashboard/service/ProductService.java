package id.teguh.training.dashboard.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import id.teguh.training.dashboard.dto.Product;
import id.teguh.training.dashboard.util.ApiUtil;
import id.teguh.training.dashboard.util.RestResponsePage;
import org.apache.tomcat.util.codec.binary.Base64;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import java.io.IOException;
import java.util.Map;

@Service
public class ProductService {
    private Logger LOGGER = LoggerFactory.getLogger(ProductService.class);

    @Autowired
    private RestTemplate restTemplate;

    @Value("${catalog.url}")
    private String catalogUrl;

    @Value("${catalog.credential}")
    private String catalogCredential;

    private HttpHeaders getHttpHeaders() {
        String base64ClientCredential = new String(Base64.encodeBase64(catalogCredential.getBytes()));
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.setContentType(MediaType.APPLICATION_JSON);
        httpHeaders.add("Authorization", "Basic " + base64ClientCredential);
        return httpHeaders;
    }

    public Product create(Product product) {
        LOGGER.info("create product: {}", product.toString());
        ResponseEntity<Product> responseEntity = restTemplate.exchange(catalogUrl + "/api/v1/products", HttpMethod.POST, new HttpEntity(product, getHttpHeaders()), Product.class);
        return responseEntity.getBody();
    }

    public Page<Product> list(Pageable pageable, String name) throws IOException {
        UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(catalogUrl + "/api/v1/products");
        builder.queryParam("name", name);
        builder.queryParam("page", pageable.getPageNumber());
        builder.queryParam("size", pageable.getPageSize());
        builder.queryParam("sort", pageable.getSort());

        ResponseEntity<RestResponsePage<Product>> responseEntity = restTemplate.exchange(builder.build().encode().toUri(), HttpMethod.GET, new HttpEntity(getHttpHeaders()), new ParameterizedTypeReference<RestResponsePage<Product>>() {});
        return responseEntity.getBody();
    }

    public Product get(String id) {
        LOGGER.info("get detail product " + id);
        UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(catalogUrl + "/api/v1/products/" + id);
        return restTemplate.exchange(builder.build().encode().toUri(), HttpMethod.GET, new HttpEntity(getHttpHeaders()), Product.class).getBody();
    }

    public void update(String id, Product product) {
        LOGGER.info("get detail product " + id);
        ObjectMapper oMapper = new ObjectMapper();
        Map request = oMapper.convertValue(product, Map.class);
        restTemplate.exchange(catalogUrl + "/api/v1/products/" + id, HttpMethod.PUT, new HttpEntity(request, getHttpHeaders()), String.class);
    }

    public void delete(String id) {
        LOGGER.info("delete product " + id);
        restTemplate.exchange(catalogUrl + "/api/v1/products/" + id, HttpMethod.DELETE, new HttpEntity(getHttpHeaders()), String.class);
    }
}
