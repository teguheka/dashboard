package id.teguh.training.dashboard.util;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.http.ResponseEntity;

import java.util.HashMap;

public class ApiUtil {

    public static Object parse(ResponseEntity<String> responseEntity) {
        if (responseEntity.getStatusCode().is2xxSuccessful()) {
            try {
                ObjectMapper mapper = new ObjectMapper();
                return mapper.readValue(responseEntity.getBody(), HashMap.class);
            } catch (Exception e) {
                throw new RuntimeException("JsonException, message: " + e.getMessage());
            }
        } else {
            throw new RuntimeException("HttpStatus: " + responseEntity.getStatusCode() + ", error: " + responseEntity.getBody());
        }
    }
}
