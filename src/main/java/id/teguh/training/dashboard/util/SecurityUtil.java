package id.teguh.training.dashboard.util;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class SecurityUtil {

    /**
     * Check user has role or not
     *
     * @return Boolean, if false user doesn't have role, otherwise have role
     */
    protected Boolean hasRole(String role) {
        SecurityContext context = SecurityContextHolder.getContext();
        Authentication authentication = context.getAuthentication();
        Collection<? extends GrantedAuthority> authorities = authentication.getAuthorities();
        return authorities.contains(new SimpleGrantedAuthority(role));
    }

    /**
     * Get info about currently logged in user
     *
     * @return UserDetails if found in the context, null otherwise
     */
    protected UserDetails getUserDetails() {
        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        UserDetails userDetails = null;
        if (principal instanceof UserDetails) {
            userDetails = (UserDetails) principal;
        }
        return userDetails;
    }

    /**
     * Check if a role is present in the authorities of current user
     *
     * @param authorities all authorities assigned to current user
     * @param role        required authority
     * @return true if role is present in list of authorities assigned to current user, false otherwise
     */
    protected Boolean isRolePresent(Collection<GrantedAuthority> authorities, String role) {
        Boolean isRolePresent = Boolean.FALSE;
        for (GrantedAuthority grantedAuthority : authorities) {
            isRolePresent = grantedAuthority.getAuthority().equals(role);
            if (isRolePresent) break;
        }
        return isRolePresent;
    }

    /**
     * List granted role present user
     *
     * @return List of Role
     */
    protected List<GrantedAuthority> getRoles(){
        List<GrantedAuthority> grantedAuthorities = new ArrayList<>();
        SecurityContext context = SecurityContextHolder.getContext();
        Authentication authentication = context.getAuthentication();
        grantedAuthorities.addAll(authentication.getAuthorities());
        return grantedAuthorities;
    }
}
