CREATE TABLE c_password_reset_token (
    id character varying(255) NOT NULL,
    expiry_date timestamp without time zone,
    token character varying(255) NOT NULL,
    id_user character varying(255) NOT NULL
);

CREATE TABLE c_security_permission (
    id character varying(255) NOT NULL,
    permission_label character varying(255) NOT NULL,
    permission_value character varying(255) NOT NULL
);

CREATE TABLE c_security_role (
    id character varying(255) NOT NULL,
    description character varying(255),
    name character varying(255) NOT NULL
);

CREATE TABLE c_security_role_permission (
    id_role character varying(255) NOT NULL,
    id_permission character varying(255) NOT NULL
);

CREATE TABLE c_security_user (
    id character varying(255) NOT NULL,
    active boolean NOT NULL,
    username character varying(255) NOT NULL,
    id_role character varying(255) NOT NULL
);

CREATE TABLE c_security_user_password (
    id_user character varying(36) NOT NULL,
    password character varying(255) NOT NULL
);

ALTER TABLE ONLY c_password_reset_token ADD CONSTRAINT c_password_reset_token_pkey PRIMARY KEY (id);
ALTER TABLE ONLY c_security_permission ADD CONSTRAINT c_security_permission_pkey PRIMARY KEY (id);
ALTER TABLE ONLY c_security_role_permission ADD CONSTRAINT c_security_role_permission_pkey PRIMARY KEY (id_role, id_permission);
ALTER TABLE ONLY c_security_role ADD CONSTRAINT c_security_role_pkey PRIMARY KEY (id);
ALTER TABLE ONLY c_security_user_password ADD CONSTRAINT c_security_user_password_pkey PRIMARY KEY (id_user);
ALTER TABLE ONLY c_security_user ADD CONSTRAINT c_security_user_pkey PRIMARY KEY (id);
ALTER TABLE ONLY c_security_user ADD CONSTRAINT uk_at8if7a9lnl90wxllb9divpdf UNIQUE (username);
ALTER TABLE ONLY c_password_reset_token ADD CONSTRAINT uk_feu70fo876metrg5g46eh21nf UNIQUE (token);
ALTER TABLE ONLY c_security_role ADD CONSTRAINT uk_hliaoojt6u3a11d8svttju10l UNIQUE (name);
ALTER TABLE ONLY c_security_permission ADD CONSTRAINT uk_k4suda9cvcsoikdgquscypmt6 UNIQUE (permission_value);
ALTER TABLE ONLY c_password_reset_token ADD CONSTRAINT uk_tf9agbefkf39bqj62wprw2vo0 UNIQUE (id_user);
ALTER TABLE ONLY c_security_user_password ADD CONSTRAINT fk80arji7i1u0styufcy8b91it5 FOREIGN KEY (id_user) REFERENCES c_security_user(id);
ALTER TABLE ONLY c_security_user ADD CONSTRAINT fke5ychpyk27l8vj47v36mrn0s1 FOREIGN KEY (id_role) REFERENCES c_security_role(id);
ALTER TABLE ONLY c_security_role_permission ADD CONSTRAINT fkg9os4isbs19ssfahravousxes FOREIGN KEY (id_role) REFERENCES c_security_role(id);
ALTER TABLE ONLY c_password_reset_token ADD CONSTRAINT fkm4d87sx4kfcxn34v23i75vh91 FOREIGN KEY (id_user) REFERENCES c_security_user(id);
ALTER TABLE ONLY c_security_role_permission ADD CONSTRAINT fknqcv2qdac1phe20qqnyi6n1n FOREIGN KEY (id_permission) REFERENCES c_security_permission(id);