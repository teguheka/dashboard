package id.teguh.training.dashboard;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class DashboardApplicationTests {

	@Autowired private PasswordEncoder passwordEncoder;

	@Test
	public void testEncodePassword(){
		System.out.println(passwordEncoder.encode("teguh"));
		Assert.assertNotNull(passwordEncoder.encode("teguh"));
		Assert.assertTrue(passwordEncoder.matches("admin", "$2a$13$/MNKNdUg.06CLje514mgJuzrzdYbEn6KsY5ti//SQVZwRzSnEUR2i"));
	}

}
